# gitlab-pipelines-dashboard

Ripped from https://github.com/mvisonneau/gitlab-ci-pipelines-exporter. In case of any problems refer to this docs.

## Requirements

- A personal access token on [gitlab.com](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) (or your own instance) with `read_api` scope
- [git](https://git-scm.com/) & [docker-compose](https://docs.docker.com/compose/)

## Run

```bash
# Clone this repository
~$ git clone https://github.com/lbulawa/gitlab-pipelines-dashboard.git
~$ cd gitlab-pipelines-dashboard

# Provide your personal GitLab API access token (needs read_api permissions)
~$ sed -i 's/your_token/<your_token>/' gitlab-ci-pipelines-exporter.yml
~$ sed -i 's/your_project_path/<your_project_path>/' gitlab-ci-pipelines-exporter.yml

# Start gitlab-ci-pipelines-exporter, prometheus and grafana containers !
~$ docker-compose up -d
Creating network "gitlab-pipelines-dashboard_default" with the default driver
Creating gitlab-pipelines-dashboard_gitlab-ci-pipelines-exporter_1 ... done
Creating gitlab-pipelines-dashboard_prometheus_1                   ... done
Creating gitlab-pipelines-dashboard_grafana_1                      ... done
```

## Use

Visit [http://localhost:3000/](http://localhost:3000/) and go through the dashboards. If you want/need to login, creds are _admin/admin_.

## Perform configuration changes

I believe it would be more interesting for you to be monitoring your own projects. To perform configuration changes, there are 2 simple steps:

```bash
# Edit the configuration file for the exporter
~$ vim gitlab-ci-pipelines-exporter.yml

# Restart the exporter container
~$ docker-compose restart gitlab-ci-pipelines-exporter
```

## Cleanup

```bash
~$ docker-compose down
```
